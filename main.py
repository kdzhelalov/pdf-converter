from builder import files_builder, excel_loader


if __name__ == "__main__":
    loaded_excel_file = excel_loader()
    if loaded_excel_file:
        files_builder(loaded_excel_file)

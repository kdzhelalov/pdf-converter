import io
import shared_resources
from reportlab.lib.pagesizes import landscape, A4
from reportlab.pdfgen import canvas
from PyPDF2 import PdfReader, PdfWriter


def add_pagination(input_doc_path, output_doc_path):
    with open(input_doc_path, 'rb') as file:
        doc_reader = PdfReader(file)
        packet = io.BytesIO()
        can = canvas.Canvas(packet, pagesize=landscape(A4))

        # Creating canvas with pagination
        for page_num in range(len(doc_reader.pages)):
            if page_num % 2 == 0:
                shared_resources.pagination_index = shared_resources.pagination_index + 1
                page = doc_reader.pages[page_num]
                page_width = float(page.mediabox[2])
                page_height = float(page.mediabox[3])
                text = str(shared_resources.pagination_index)
                text_width = can.stringWidth(text, "Helvetica", 10)
                x = (page_width - text_width) - 10
                y = 10
                can.drawString(x, y, text)
            can.showPage()
        can.save()
        packet.seek(0)

        page_numbers = PdfReader(packet)
        paginated_doc = PdfWriter()

        # Merging pagintaion canvas with current doc
        for page_num in range(len(doc_reader.pages)):
            page = doc_reader.pages[page_num]
            page.merge_page(page_numbers.pages[page_num])
            paginated_doc.add_page(page)

        paginated_doc.write(output_doc_path)

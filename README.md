Here is a program that converts and merges different files into one pdf file.

Supported file types: .doc, .docx, .xls, .xlsx, .jpg, .jpeg, .png, .html, .pdf

IMPORTANT: 
- Excel and Word must be installed for processing .doc, .docx and .xls, .xlsx
- wkhtmltopdf must be installed for .html


It iterates over an Excel file that contains source paths of desired files and when it catches the path at the destination column, it starts doing its magic and afterward, you get a merged pdf file. The program works by file blocks when you can set the order of each file. Block of files considered ended when a value in the destination column is found. 
Additionally, the program adds a blank page to each document if its number of pages is odd. Also, it numerates odd pages if the value in the pagination column exists. (If you don't need this functionality just comment the corresponding piece of code in main.py)

Tested on Windows 10 Pro 22H2 with Python 3.11.4

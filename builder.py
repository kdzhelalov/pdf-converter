import tkinter
import os
import sys
import convert
import shutil
import shared_resources
from tkinter import filedialog
from openpyxl import load_workbook
from operator import itemgetter
from PyPDF2 import PdfMerger, PdfWriter, PdfReader
from pagination import add_pagination


# Create temp folder
temp = os.path.join(os.getcwd(), "tempdocs")
if not os.path.exists(temp):
    os.makedirs(temp)

# Upload main excel file with documents info
def excel_loader():
    try:
        root = tkinter.Tk()
        root.withdraw()
        excel_path = filedialog.askopenfilename(title="Choose Excel file", filetypes=[("Excel Files", "*.xlsx")])
        excel_file = load_workbook(excel_path).active
    except Exception as e:
        print(f'Unsuccessfully uploading main excel file: {e}')
        return False
    else:
        print('Excel file successfully uploaded!')
        return excel_file
    finally:
        root.destroy()


def files_builder(excel_file):
    documents = {}
    skip_block_of_files = False

    for row in excel_file.iter_rows(min_row=2, values_only=True):
        doc_path, order, destination, pagination = row[0], row[1], row[2], row[3]

        if skip_block_of_files:
            if destination:
                skip_block_of_files = False
            continue

        if doc_path: 
            if os.path.exists(doc_path):
                print(f"Doc {doc_path} exists!")
                documents[order] = [doc_path, pagination]
            else:
                print(f"Doc {doc_path} does NOT exist!")
                skip_block_of_files = True
                documents = {}
                print("---!!!---SKIP BLOCK OF FILES---!!!---")
                continue

            if destination:
                final_doc = destination
                result_folder_name = os.path.dirname(final_doc)
                if not os.path.exists(result_folder_name):
                    os.makedirs(result_folder_name)

                ordered_documents = {key: value for key, value in sorted(documents.items())}
                pdf_merger = PdfMerger()
                valid_block = True

                for doc in ordered_documents:
                    doc_path = ordered_documents[doc][0]
                    doc_extension = (os.path.splitext(doc_path)[1]).lower()
                    doc_name = os.path.basename(doc_path)

                    converted_doc_path = temp + '\\' + doc_name + ".pdf"

                    try:
                        if doc_extension == '.pdf':
                            shutil.copy(doc_path, converted_doc_path)
                    
                        elif doc_extension in ['.doc', '.docx']:
                            convert.word_to_pdf(doc_path, converted_doc_path)

                        elif doc_extension in ['.xls', '.xlsx']:
                            convert.excel_to_pdf(doc_path, converted_doc_path)

                        elif doc_extension in ['.jpg', '.png', 'jpeg']:
                            convert.img_to_pdf(doc_path, converted_doc_path)

                        elif doc_extension in ['.html']:
                            convert.html_to_pdf(doc_path, converted_doc_path)

                        # Adding blank page to the end of doc if it has odd number of page
                        converted_doc = PdfReader(converted_doc_path)
                        if len(converted_doc.pages) % 2 != 0:
                            new_converted_doc = PdfWriter()
                            new_converted_doc.clone_document_from_reader(converted_doc)
                            new_converted_doc.add_blank_page()
                            new_converted_doc.write(converted_doc_path)
                        
                        # Adding paginatin to doc
                        if ordered_documents[doc][1]:
                            add_pagination(converted_doc_path, converted_doc_path)

                    except Exception as e:
                        print(f"Error processing {doc_name}: {e}")
                        print("---!!!---SKIP---!!!---")
                        valid_block = False
                        break
                    else:
                        print(f"Successfully processed {doc_name}")
                        doc_for_merging = PdfReader(converted_doc_path)
                        pdf_merger.append(doc_for_merging)
                
                if valid_block:
                    pdf_merger.write(final_doc)
                    pdf_merger.close()
                    print(f'---!!!---Final doc succssefully saved---!!!--- {final_doc}')
                documents, ordered_documents, shared_resources.pagination_index = {}, {}, 0

    shutil.rmtree(temp)

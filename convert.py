import img2pdf
import pdfkit
from win32com import client
from PIL import Image


WKHTMLTOPDF_PATH = 'C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe'


def word_to_pdf(input, output):
    word = client.DispatchEx("Word.Application")
    word.Visible = False
    doc = word.Documents.Open(input)
    doc.SaveAs(output, FileFormat=17)
    doc.Close()
    word.Quit()


def excel_to_pdf(input, output):
    excel = client.DispatchEx('Excel.Application')
    excel.Visible = False
    workbook = excel.Workbooks.Open(input)
    workbook.ExportAsFixedFormat(0, output)
    workbook.Close()
    excel.Quit()


def img_to_pdf(input, output):
    image = Image.open(input)
    pdf_bytes = img2pdf.convert(image.filename)
    file = open(output, "wb")
    file.write(pdf_bytes)
    image.close()
    file.close()


def html_to_pdf(input, output):
    config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)
    pdfkit.from_file(input, output_path=output, configuration=config)
